"""
This script converts a xlsx file to a csv file
"""

import argparse
import csv
import sys
import os.path
import xlrd


def main():
    """
    Handles user arguments
    """
    parser = argparse.ArgumentParser(description="Convert .xlsx file to .csv")
    parser.add_argument("-i", "--input", required=True, action="store", dest="input")
    parser.add_argument("-o", "--output", action="store", dest="output")
    args = parser.parse_args()

    conversion(args.input, args.output)


def conversion(input_filename, output_filename=None):
    """
    Converts xlsx to csv
    :param input_filename: name of the .xlsx file
    :param output_filename: name of the output .csv file
    """
    try:
        workbook = xlrd.open_workbook(input_filename)
    except FileNotFoundError as err:
        print(err)
        print("Please verify the input file exists")
        sys.exit(1)
    except xlrd.XLRDError as err:
        print(err)
        print("Please provide a proper .xlsx file")
        sys.exit(1)

    output_filename = output_filename if output_filename is not None \
        else os.path.splitext(input_filename)[0] + ".csv"

    sheet = workbook.sheet_by_name(workbook.sheet_names()[0])
    your_csv_file = open(output_filename, 'w')
    writer = csv.writer(your_csv_file, quoting=csv.QUOTE_ALL)

    for rownum in range(sheet.nrows):
        writer.writerow(sheet.row_values(rownum))

    your_csv_file.close()


if __name__ == "__main__":
    main()
